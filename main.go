package main

import (
	"fmt"

	ui "github.com/VladimirMarkelov/clui"
)

const rowCount = 15

func changeTheme(btn *ui.Button, tp int) {
	items := ui.ThemeNames()
	dlgType := ui.SelectDialogRadio
	if tp == 1 {
		dlgType = ui.SelectDialogList
	}

	curr := -1
	for i, tName := range items {
		if tName == ui.CurrentTheme() {
			curr = i
			break
		}
	}

	selDlg := ui.CreateSelectDialog("Choose a theme", items, curr, dlgType)
	selDlg.OnClose(func() {
		switch selDlg.Result() {
		case ui.DialogButton1:
			idx := selDlg.Value()
			if idx != -1 {
				ui.SetCurrentTheme(items[idx])
			}
		}

		btn.SetEnabled(true)
		// ask the composer to repaint all windows
		ui.PutEvent(ui.Event{Type: ui.EventRedraw})
	})
}

func CreateTable(view *ui.Window, tableFrame *ui.Frame) *ui.TableView {

	dataTableView := ui.CreateTableView(tableFrame, 25, 12, 1)
	ui.ActivateControl(view, dataTableView)
	dataTableView.SetShowLines(true)
	dataTableView.SetShowRowNumber(true)
	dataTableView.SetRowCount(rowCount)
	cols := []ui.Column{
		ui.Column{Title: "Text", Width: 5, Alignment: ui.AlignLeft},
		ui.Column{Title: "Number", Width: 10, Alignment: ui.AlignRight},
		ui.Column{Title: "Misc", Width: 12, Alignment: ui.AlignCenter},
		ui.Column{Title: "Long", Width: 50, Alignment: ui.AlignLeft},
		ui.Column{Title: "Last", Width: 8, Alignment: ui.AlignLeft},
	}
	dataTableView.SetColumns(cols)
	colCount := len(cols)

	values := make([]string, rowCount*colCount)
	for r := 0; r < rowCount; r++ {
		for c := 0; c < colCount; c++ {
			values[r*colCount+c] = fmt.Sprintf("%v:%v", r, c)
		}
	}

	dataTableView.OnDrawCell(func(info *ui.ColumnDrawInfo) {
		info.Text = values[info.Row*colCount+info.Col]
	})
	return dataTableView
}

func createView() {

	view := ui.AddWindow(0, 0, 20, 7, "Kubernets UI")

	// All frames definitions
	frmLeft := ui.CreateFrame(view, 8, 4, ui.BorderThin, 1)
	frmLeft.SetPack(ui.Vertical)
	frmLeft.SetGaps(ui.KeepValue, 1)
	frmLeft.SetPaddings(1, 1)

	ui.ActivateControl(view, frmLeft)

	controls := ui.CreateFrame(frmLeft, 8, 1, ui.BorderThin, ui.Fixed)
	controls.SetGaps(1, ui.KeepValue)

	tableFrame := ui.CreateFrame(frmLeft, 8, 1, ui.BorderThin, ui.Fixed)
	// Initiate empty table
	centralTable := CreateTable(view, tableFrame)
	centralTable.Visible()

	// Buttons frame=controls
	btnNamespace := ui.CreateButton(controls, ui.AutoSize, 4, "Select Namespace", ui.Fixed)
	btnNamespace.SetShadowType(ui.ShadowHalf)
	btnPods := ui.CreateButton(controls, ui.AutoSize, 4, "Pods", ui.Fixed)
	btnPods.SetShadowType(ui.ShadowHalf)
	btnServices := ui.CreateButton(controls, ui.AutoSize, 4, "Services", ui.Fixed)
	btnServices.SetShadowType(ui.ShadowHalf)
	btnDeployments := ui.CreateButton(controls, ui.AutoSize, 4, "Deployments", ui.Fixed)
	btnDeployments.SetShadowType(ui.ShadowHalf)

	// Bottom frame, inclose "quit" button
	frmBottom := ui.CreateFrame(frmLeft, 8, 1, ui.BorderThin, ui.Fixed)
	frmBottom.SetPaddings(1, 1)
	frmBottom.SetGaps(1, ui.KeepValue)

	// frame that pushe "quit" button to the right.
	ui.CreateFrame(frmBottom, 1, 1, ui.BorderNone, 1)
	btnQuit := ui.CreateButton(frmBottom, ui.AutoSize, 4, "Quit", ui.Fixed)
	btnQuit.SetShadowType(ui.ShadowHalf)

	// Buttons functions

	// Select namespace
	btnNamespace.OnClick(func(ev ui.Event) {
		btnNamespace.SetEnabled(false)
		tp := 1
		changeTheme(btnNamespace, tp)
	})

	// List Pods
	btnPods.OnClick(func(ev ui.Event) {
		centralTable.Destroy()
	})

	// List Services
	btnServices.OnClick(func(ev ui.Event) {
		centralTable.Destroy()
		centralTable = CreateTable(view, tableFrame)
		//centralTable.Visible()
	})

	// List Deployments
	btnDeployments.OnClick(func(ev ui.Event) {
		centralTable.Destroy()
	})

	// Quit TUI
	btnQuit.OnClick(func(ev ui.Event) {
		go ui.Stop()
	})

	// table
}

func mainLoop() {
	// Every application must create a single Composer and
	// call its intialize method
	ui.InitLibrary()
	defer ui.DeinitLibrary()

	ui.SetThemePath("themes")

	createView()

	// start event processing loop - the main core of the library
	ui.MainLoop()
}

func main() {
	mainLoop()
}
